//
// Created by fang on 20-5-5.
//

#ifndef SEGMENTATION_SEGEVENT_H
#define SEGMENTATION_SEGEVENT_H

#include "segbuf.h"
#include <event2/event.h>

typedef struct s_context{
    int fd; //上下文读取fd
    seg_buf* read_buf; //数据读取缓存
    seg_buf* write_buf; //写缓存数据
    int writed_len; //写缓存已经写入长度
    struct event* ev;
    void* data; //外部传递的上下文数据
} context;

typedef struct s_seg_events {
    context** contexts;
    unsigned int contexts_size;
    unsigned int contexts_free_size;
    struct event_base *events;
} seg_events;

void seg_event_init();
void seg_event_add(int fd, short events, event_callback_fn , void *);
void seg_event_remove(context *);

#endif //SEGMENTATION_SEGEVENT_H
