//
// Created by fang on 20-5-6.
//

#ifndef SEGMENTATION_SERVER_H
#define SEGMENTATION_SERVER_H

#include "segevent.h"

#define READ_LEN 2048

struct client {
    int fd;
    struct bufferevent *buf_ev;
    char input[1024*5];
    long int input_need_len;
};

void start_server();
void reset_client(struct client* client);
//void on_accept(int fd);

#endif //SEGMENTATION_SERVER_H
