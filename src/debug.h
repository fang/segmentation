/*
 *  segmentation
 *
 *  license: GPL2
 *  Copyright (C) i@liufang.org.cn
 *  Author: fang.liu
 */

#ifndef DEBUG_H_
#define DEBUG_H_

void seg_debug(char* str);
void seg_debugx(char* str);
void seg_debugxn(char* str, int n);

#endif /* DEBUG_H_ */
