//
// Created by fang on 20-5-10.
//

#ifndef SEGMENTATION_LINK_H
#define SEGMENTATION_LINK_H
//数据节点
typedef struct s_seg_link_node {
    struct s_seg_link_node* prev;
    struct s_seg_link_node* next;
    void* data;
} seg_link_node;

//链表头
typedef struct s_seglink {
    unsigned int len;
    struct s_seg_link_node* lnode; //已使用节点
    struct s_seg_link_node* rnode; //最后一个使用的节点
  //  struct s_seg_link_node* free_node; //未使用节点
} seg_link;


seg_link* seg_link_init(unsigned int size);
//void seg_link_resize(unsigned int size);
seg_link_node* seg_link_lget();
#endif //SEGMENTATION_LINK_H
