/*
 *  segmentation
 *
 *  license: GPL2
 *  Copyright (C) i@liufang.org.cn
 *  Author: fang.liu
 */

#include <malloc.h>
#include "str.h"

//创建一个seg字符串
seg_str* create_seg_str(const char* str)
{
	seg_str* p_seg_str = (seg_str*)malloc(sizeof(seg_str));
	char* src_str;
	int len = strlen(str);
	p_seg_str->str = (char*) malloc(sizeof(char)*len+1);
	strcpy(p_seg_str->str, str);
	p_seg_str->len = len;
	return p_seg_str;
}

//链接字符串
seg_str* cat_str(seg_str *des, seg_str * src)
{
	char *str = NULL;
	if(des == NULL) {
		des = create_seg_str(src->str);
	} else {
		//重新分配内存, 包含1个空格一个\0
		//printf("src len:%d, des len: %d, realloc len:%d \r\n",src->len, des->len, src->len + des->len + 2);
		des->str = realloc(des->str, sizeof(char) * src->len + des->len + 2);
		str = des->str;
		str += des->len;
		*str = ' ';
		str++;
		strncpy(str, src->str, src->len);
		str += src->len + 1;
		*str = '\0';
		des->len += src->len +1;
	}
	return des;
}

/**
 * 自定义字符串结构追加字符串
 *
 * @param des
 * @param str
 * @return
 */
seg_str* append_char(seg_str *des, char* str)
{
    char *p;
	int len = strlen(str);
	des->str = realloc(des->str, len + des->len + 1);
	p+=len + des->len + 1;
    strncpy(p, str, len + 1);
	des->len = des->len+len;
	return des;
}

//free string
void free_str(seg_str *str)
{
	free(str->str);
	free(str);
}

