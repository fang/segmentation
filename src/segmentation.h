/*
 *  segmentation
 *
 *  license: GPL2
 *  Copyright (C) i@liufang.org.cn
 *  Author: fang.liu
 */
#ifndef SEG_H_
#define SEG_H_

#include <stdlib.h>
#include "str.h"
#include "index.h"
#include "debug.h"
#include "segbuf.h"

#define SEGMENTATION_VERSION "1.0"
//网络传输结构体
typedef struct s_msg {
	int8_t len;
	char data[];
} msg;

//匹配结果结构
typedef struct s_result_node {
	word* word;
	struct s_result* next;
} result_node;

//匹配结果头
typedef struct s_result {
	int count;
	result_node* node;
	result_node* last;
} result;

//分词
result* seg(const char* str);
void add_result_node(result* result, word* w);
result* init_result();
//free result
void free_result(result* r);
//打印匹配结果
void print_result(result* r);
//字符串的方式返回结果(单词之间用空格分割单词)
seg_buf* string_result(result* r);

#endif /* SEG_H_ */
