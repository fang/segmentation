//
// Created by fang on 20-5-24.
//

#ifndef SEGMENTATION_SEGMENTATIONCLI_H
#define SEGMENTATION_SEGMENTATIONCLI_H

#include "segmentation.h"

#define SERVER_DEFAULT_PORT 12345
#define SERVER_DEFUALT_HOST "127.0.0.1"

int cli_connect();
int cli_send(int fd, msg* m);

#endif //SEGMENTATION_SEGMENTATIONCLI_H
