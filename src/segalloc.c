//
// Created by fang on 20-5-23.
//

#include <malloc.h>
#include <stdlib.h>
#include "segalloc.h"

//out of memory
static void oom_default(size_t size) {
    fprintf(stderr, "segmalloc: Out of memory trying to allocate %zu bytes\n", size);
    fflush(stderr);
    abort();
}

//分配内存
//TODO 内存记数
void *segmalloc(size_t size) {
    void *ptr = malloc(size);
    if(!ptr) {
        oom_default(size);
    }
    return ptr;
}

//call memory
void *segcalloc(size_t size) {
    void *ptr = calloc(1, size);
    if(!ptr) {
        oom_default(size);
    }
    return ptr;
}

//free
void segfree(void* ptr) {
    if(ptr == NULL) return;
    free(ptr);
}