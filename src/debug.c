/*
 *  segmentation
 *
 *  license: GPL2
 *  Copyright (C) i@liufang.org.cn
 *  Author: fang.liu
 */

#include <stdio.h>
#include <string.h>

void seg_debug(char* str)
{
    printf("%s\r\n", str);
}

void seg_debugx(char* str)
{
    printf("len: %d, 16 data:", strlen(str));
    for (int j = 0; j < strlen(str); ++j) {
        printf("%x ", str[j]);
    }
    printf("\n");
}

/**
 * 打印制定字节
 *
 * @param str
 * @param n
 */
void seg_debugxn(char* str, int n)
{
    printf("print len: %d, 16 data:", n);
    for (int j = 0; j < n; ++j) {
        printf("%x ", str[j]);
    }
    printf("\n");
}