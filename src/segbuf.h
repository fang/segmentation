//
// Created by fang on 20-5-5.
//

#ifndef SEGMENTATION_SEGBUF_H
#define SEGMENTATION_SEGBUF_H

#include "str.h"

typedef struct s_seg_buf {
    char* data;
    int data_len;
    int len;
} seg_buf;

#define SEG_BUF_SIZE 2048

seg_buf* seg_buf_init();
void seg_buf_append(seg_buf* buf, char* str);
void seg_buf_appendn(seg_buf* buf, char* str, int n);
void seg_buf_append_str(seg_buf* buf, seg_str* str);
void seg_buf_free(seg_buf* buf);

#endif //SEGMENTATION_SEGBUF_H
