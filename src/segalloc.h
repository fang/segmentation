//
// Created by fang on 20-5-23.
//

#include <stddef.h>

#ifndef SEGMENTATION_SEGALLOC_H
void *segmalloc(size_t size);
void *segcalloc(size_t size);
void segfree(void* ptr);

#define SEGMENTATION_SEGALLOC_H
#endif //SEGMENTATION_SEGALLOC_H
